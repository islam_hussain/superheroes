﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleScreenText : MonoBehaviour {

	CanvasGroup canvasGroup;

	// Use this for initialization
	void Start () {
		canvasGroup = gameObject.GetComponent<CanvasGroup>();
		canvasGroup.alpha = 0.0f;
		StartCoroutine(StartFadingIn());
	}

	IEnumerator StartFadingIn() {
		yield return new WaitForSeconds(0.5f);
		for(float i = 0; i <= 1f; i = i + 0.4f * Time.deltaTime) {
			canvasGroup.alpha = i;
			yield return null;
		}
	}
}
