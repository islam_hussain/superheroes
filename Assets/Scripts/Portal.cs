﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {

	public Transform PortalExit;
	
	void OnTriggerEnter(Collider collisionObject) {
		Debug.Log("Trigger Entered");
		 collisionObject.transform.position = PortalExit.position;
	}
}
