using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

public class Shard : MonoBehaviour {


	public VRInput vrInput;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(OVRInput.GetDown(OVRInput.Button.One))
			HandleClick(); 
		transform.Rotate(0, 20f * Time.deltaTime, 0);
	}

	void OnEnable() {
		//vrInput.OnClick += HandleClick;
	}

	void HandleClick() {
		StartCoroutine(StartShrinking());
	}

	IEnumerator StartShrinking() {
		Vector3 temp = transform.localScale;
		while(transform.localScale.x > 0) {
			temp.x -= 0.1f;
			temp.y -= 0.1f;
			temp.z -= 0.1f;
			transform.localScale = temp;
		}
		SceneManager.LoadScene(1);
		yield return new WaitForEndOfFrame();
	}

}
