﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Jumper: MonoBehaviour {

	float strength = 30000.0f;

	void Update() {
		if(OVRInput.GetDown(OVRInput.Button.One) || Input.GetKey(KeyCode.W)) {
			Jump();
		}
	}

	void Jump() {
		gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * strength);
	}
}