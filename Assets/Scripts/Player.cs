﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	// Use this for initialization
	public void Die() {
		Application.Quit();
	}

	void OnTriggerEnter(Collider collisionObject) {
		Debug.Log("Trigger Entered");
		if(collisionObject.gameObject.name == "PortalEnter") {
			transform.position = collisionObject.transform.position;
		}
	}
}
