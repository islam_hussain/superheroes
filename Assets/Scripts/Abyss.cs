﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abyss : MonoBehaviour {

	void OnCollisionEnter() {
		KillPlayer();
	}

	void KillPlayer() {
		GameObject.FindWithTag("Player").GetComponent<Player>().Die();
	}
}
