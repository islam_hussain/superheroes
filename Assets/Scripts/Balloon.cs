﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : MonoBehaviour {

	private float balloonDriftZ;
	private float balloonDriftY;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		MoveBalloon();
	}

	void MoveBalloon() {
		balloonDriftZ = balloonDriftZ + Random.Range(0.001f, 0.01f) * Time.deltaTime;
		balloonDriftY = Mathf.Sin(Time.deltaTime);
		transform.Translate(0, balloonDriftY, balloonDriftZ);
	}
}
