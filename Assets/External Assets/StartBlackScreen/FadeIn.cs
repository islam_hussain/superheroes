﻿using UnityEngine;
using System.Collections;

public class FadeIn : MonoBehaviour {
	private float fadeIn;
	// Use this for initialization
	void Start () {
		fadeIn = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
		if (fadeIn <= 0.0f) {
						this.enabled = false;
						Destroy (gameObject);
		}
		else {
			fadeIn -= (Time.deltaTime/3);
			transform.GetComponent<Renderer>().material.SetColor ("_Color", new Color (1,1,1,fadeIn));
		}
	}
}
